File Name: Scrolling Animation Collection
Version: 1.0
For: TI-83+
Programmed By: Jeremy Duenas
Description: A couple of scrolling animations I made.

Contains:
Snow (Looks like snow)
DNA (Looks like DNA twirling)
Wave (Looks like a wave)
Matrix (Scrolling 1's and 0's)
Whatever (I'm not sure just what this one is)


Contact Information:

Email- ToxicityJ@gmail.com
AIM- Duenos14
Yahoo- Toxicity2788

